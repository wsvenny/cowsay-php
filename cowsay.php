<?php
/* This library gives leightweighted PHP version of UNIX script cowsay.
 * Usage:
 * 	1. include this library
 * 	2. use function cowsay($text, $width=40, $model=null, $eyes=null, $thoughts="\\", $tongue="  ")
 * 		* $text		= what should cow say? (\n works)
 * 		* $width	= maximal line length for text (wraps on <space>)
 * 		* $model	= model; default: random model
 * 		* $eyes		= eyes characters - 2 characters expected; not supported by every model; default: random from prepared set
 * 		* $thoughts	= char used to draw line from model to bubble
 * 		* $tongue	= tongue - 2 characters expected; not supported by every model; default: no tongue
 * 		- returns	list (array) of lines of complete picture
 *
 * Simplest usage:
 * "<pre>".implode("\n", cowsay($some_wise_text)."</pre>"
 *
 * ... or as a webservice
 */


// set to true to allow this script to run standalone as a webservice
define("ALLOW_WEBSERVICE", true);


function _get_model($key, $just_list_keys = false) {
	$MODELS = [
"sheep" =>
'  $thoughts
   $thoughts
       __     
      U$eyesU\\.\'@@@@@@`.
      \\__/(@@@@@@@@@@)
           (@@@@@@@@)
           `YY~~~~YY\'
            ||    ||',
"head-in" =>
'    $thoughts
     $thoughts
    ^__^         /
    ($eyes)\\_______/  _________
    (__)\\       )=(  ____|_ \\_____
     $tongue ||----w |  \\ \\     \\_____ |
        ||     ||   ||           ||',
"ghostbusters" =>
'          $thoughts
           $thoughts
            $thoughts          __---__
                    _-       /--______
               __--( /     \\ )XXXXXXXXXXX\\v.
             .-XXX(   O   O  )XXXXXXXXXXXXXXX-
            /XXX(       U     )        XXXXXXX\\
          /XXXXX(              )--_  XXXXXXXXXXX\\
         /XXXXX/ (      O     )   XXXXXX   \\XXXXX\\
         XXXXX/   /            XXXXXX   \\__ \\XXXXX
         XXXXXX__/          XXXXXX         \\__---->
 ---___  XXX__/          XXXXXX      \\__         /
   \\-  --__/   ___/\\  XXXXXX            /  ___--/=
    \\-\\    ___/    XXXXXX              \'--- XXXXXX
       \\-\\/XXX\\ XXXXXX                      /XXXXX
         \\XXXXXXXXX   \\                    /XXXXX/
          \\XXXXXX      >                 _/XXXXX/
            \\XXXXX--__/              __-- XXXX/
             -XXXXXXXX---------------  XXXXXX-
                \\XXXXXXXXXXXXXXXXXXXXXXXXXX/
                  ""VXXXXXXXXXXXXXXXXXXV""',
"sodomized" =>
'      $thoughts                _
       $thoughts              (_)
        $thoughts   ^__^       / \\
         $thoughts  ($eyes)\\_____/_\\ \\
            (__)\\       ) /
             $tongue ||----w ((
                ||     ||>> ',
"tux" =>
'   $thoughts
    $thoughts
        .--.
       |o_o |
       |:_/ |
      //   \\ \\
     (|     | )
    /\'\\_   _/`\\
    \\___)=(___/',
"cow" =>
'        $thoughts   ^__^
         $thoughts  ($eyes)\\_______
            (__)\\       )\\/\\
             $tongue ||----w |
                ||     ||',
"daemon" =>
'   $thoughts         ,        ,
    $thoughts       /(        )`
     $thoughts      \\ \\___   / |
            /- _  `-/  \'
           (/\\/ \\ \\   /\\
           / /   | `    \\
           O O   ) /    |
           `-^--\'`<     \'
          (_.)  _  )   /
           `.___/`    /
             `-----\' /
<----.     __ / __   \\
<----|====O)))==) \\) /====
<----\'    `--\' `.__,\' \\
             |        |
              \\       /
        ______( (_  / \\______
      ,\'  ,-----\'   |        \\
      `--{__________)        \\/',
"elephant" =>
' $thoughts     /\\  ___  /\\
  $thoughts   // \\/   \\/ \\\\
     ((    O O    ))
      \\\\ /     \\ //
       \\/  | |  \\/ 
        |  | |  |  
        |  | |  |  
        |   o   |  
        | |   | |  
        |m|   |m|',
"moose" =>
'  $thoughts
   $thoughts   \\_\\_    _/_/
    $thoughts      \\__/
           ($eyes)\\_______
           (__)\\       )\\/\\
            $tongue ||----w |
               ||     ||'
    
];

	if($just_list_keys)
		return array_keys($MODELS);
	return ($key && isset($MODELS[$key])) ? $MODELS[$key] : $MODELS[array_rand($MODELS)];
}

function _text2lines($txt, $width=40) {
	$lns = explode("\n", $txt);
	$lines = [];
	foreach($lns as $i => $l) {
		while(mb_strlen($l) > $width) {
			$index = strrpos(substr($l, 0, $width), " ");
			$index = ($index === false) ? $width : $index;
			$lines[] = substr($l, 0, $index);
			$l = substr($l, $index+1);
		}
		$lines[] = $l;
	}
	return $lines;
}

function _normalize_str($str, $len) {
	return $str.str_repeat(" ", $len - mb_strlen($str));
}

function _enbubble_lines($lns) {
	if(count($lns) == 1)
		return [" ".str_repeat("_", mb_strlen($lns[0])+2)." ", "< $lns[0] >", " ".str_repeat("-", mb_strlen($lns[0])+2)." "];
	
	$maxlen = 0;
	foreach($lns as $l)
		if($maxlen < mb_strlen($l))
			$maxlen = mb_strlen($l);

	$lines = [" ".str_repeat("_", $maxlen+2)." ",
			"/ "._normalize_str($lns[0], $maxlen)." \\"];
	for($i = 1; $i < count($lns)-1; $i++)
		$lines[] = "| "._normalize_str($lns[$i], $maxlen)." |";
	$lines[] = "\\ "._normalize_str($lns[count($lns)-1], $maxlen)." /";
	$lines[] = " ".str_repeat("-", $maxlen+2)." ";
	return $lines;
}

function cowsay($text, $width=40, $model=null, $eyes=null, $thoughts='\\', $tongue='  ') {
	$EYE_LIB = ["oo", "..", "OO", "00", "Oo", "--"];
	if(!$eyes) $eyes = $EYE_LIB[array_rand($EYE_LIB)];

// <TAB>s expanding
	$text = str_replace("\t", "    ", $text);

	$lines = _enbubble_lines(_text2lines($text, $width));
	$m = str_replace('$tongue', $tongue, str_replace('$thoughts', $thoughts, str_replace('$eyes', $eyes, _get_model($model))));
	return array_merge($lines, explode("\n", $m));
}

function help() {
	return "
                *** Web service for cowsay ***
                ******************************

 # version: 1.1
 # params (taken from GET or POST):
    * text (what cow says; TABs are expanded to 4 SPACEs; default: this help)
    * width (max line length; default: 100)
    * eyes (eyes of the cow - 2 exactly characters; not every model accepts this)
    * tongue (tongue of the cow - 2 exactly characters; not every model accepts this)
    * model (model of the cow; variants listed below; default: random)
    * thoughts (character to draw the line between cow and text bubble)

 # models:
	[".implode(", ", _get_model(null, true))."]
";
}


### WEB SERVICE PART ###
if(ALLOW_WEBSERVICE && count(get_included_files()) == 1) {
	$data = $_GET + $_POST;
	$text = isset($data['text']) ? $data['text'] : help();
	$eyes = isset($data['eyes']) ? $data['eyes'] : null;
	$model = isset($data['model'])? $data['model'] : null;
	$width = isset($data['width']) ? $data['width'] : 100;
	$tongue = isset($data['tongue']) ? $data['tongue'] : '  ';
	$thoughts = isset($data['thoughts']) ? $data['thoughts'] : "\\";

	$content = implode("\n", cowsay($text, $width, $model, $eyes, $thoughts, $tongue));
	header("Content-Type: text/plain");
	header("Content-Length: ".strlen($content));
	print $content;
}

?>
