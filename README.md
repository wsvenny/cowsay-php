# COWSAY in PHP #

### What the hell is that? ###

This is very lightweight library implementing shell cowsay in PHP. It can also work as a web service.

* Version 1.1 (3 new models imported) 
* [Demo](http://cowsay.sveny.eu/cowsay.php?model=sodomized&eyes=Oo)

Actually it's better than original, because it:

* doesn't ignore newlines and tabs (tabs are expanded to 4 spaces)
* accepts UTF8

### Requirements ###

* PHP 5+

### Usage ###

Usage is very simple:

**As a library:**

  * place it somewhere
  * include it
  * use function cowsay (params and description listed in comments)

**As a web service:**

  * place it somewhere
  * make sure ALLOW_WEBSERVICE is set true
  * use it, help will appear if no text is submitted

### Result ###


```

 ____________________________________ 
/ Hi, I'm a cow and I can say almost \
\ anything!                          /
 ------------------------------------ 
        *   ^__^
         *  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```